module.exports = {
    transpileDependencies: [
      'vuetify'
    ],
    css: {
      loaderOptions: {
        sass: {
          prependData: '@import "@/styles/scss/_variables.scss";',
        },
      },
    },
  };