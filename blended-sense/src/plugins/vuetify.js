import '@mdi/font/css/materialdesignicons.css';
import { createVuetify } from 'vuetify';

export default createVuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#65D6EB',
        secondary: '#02E648',
        accent: '#FED4CC',
        error: '#FF0808',
        info: '#00B0D1',
        success: '#00C48C',
        warning: '#f2c94c',
        primaryBtn: '00b2d5',
      },
    },
  },
});
